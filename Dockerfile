FROM openjdk:8-jdk-alpine
RUN addgroup -S jency.saral && adduser -S jency.saral -G jency.saral
USER jency.saral:jency.saral
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]